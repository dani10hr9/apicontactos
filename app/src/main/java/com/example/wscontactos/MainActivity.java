package com.example.wscontactos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ResponseCallback {
    private ListView listContactos;
    private Button botonActualizar;
    private ArrayAdapter<String> adaptador;
    private ArrayList<String> contactos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listContactos = findViewById(R.id.listContactos);
        botonActualizar = findViewById(R.id.btn);

        botonActualizar.setOnClickListener(this);

        adaptador = new ArrayAdapter(this, android.R.layout.simple_list_item_1, contactos);
        listContactos.setAdapter(adaptador);

        new GetContactosAsyncTask("http://192.168.43.32/contactosApi/api/contactos", this).execute();
    }
    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.btn){
            new GetContactosAsyncTask("http://192.168.43.32/contactosApi/api/contactos",this).execute();
        }
    }

    @Override
    public void onResnponse(ArrayList<String> contactos) {
        //Para limpiar la lista
        this.contactos.clear();
        this.contactos.addAll(contactos);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adaptador.notifyDataSetChanged();
            }
        });


    }

}
