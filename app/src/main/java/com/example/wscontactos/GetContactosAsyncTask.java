package com.example.wscontactos;

import android.os.AsyncTask;

import com.example.wscontactos.Modelos.Contactos;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class GetContactosAsyncTask extends AsyncTask<Void, Void, Void> {

    private String url;
    private ResponseCallback callback;

    public GetContactosAsyncTask(String url, ResponseCallback callback)
    {
        this.url = url;
        this.callback = callback;
    }

    @Override
    protected Void doInBackground(Void... voids) {

        try {
            URL uri = new URL(url);
            HttpURLConnection conexion = (HttpURLConnection) uri.openConnection();
            conexion.setRequestMethod("GET");
            InputStream entradaDatos = conexion.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(entradaDatos));
            StringBuilder respuesta = new StringBuilder();
            String linea="";
            while ((linea = reader.readLine()) != null)
                respuesta.append(linea);
            JSONArray contactosJso = new JSONArray(respuesta.toString());
            ArrayList<String> contactosArray = new ArrayList<>();
            for(int y=0; y<contactosJso.length(); y++)
            {
                //Aquí tenemos cada uno de nuestros objetos tipo contactos
                JSONObject contactosJson = contactosJso.getJSONObject(y);

                //Trae los datos
                int id = contactosJson.getInt("id");
                String nombre = contactosJson.getString("nombre");
                String tipoContacto = contactosJson.getString("tipoContacto");
                String numero = contactosJson.getString("numero");


                //Crear objeto uando la clase contacto
                //Llenar cada uno a traves de sus propiedades
                //para enviar a la clase

                Contactos contacto = new Contactos();
                contacto.setId(id);
                contacto.setNombre(nombre);
                contacto.setTipoContacto(tipoContacto);
                contacto.setTipoContacto(numero);


                contactosArray.add(contacto.getNombre());
            }
            callback.onResnponse(contactosArray);
            System.out.println(respuesta.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}
